from host_name import HOST_NAME


room_name_map = {
    "RSP4674": "gray",
    "RSP4673": "rose"
}

ROOM_NAME = room_name_map[HOST_NAME]

IM_WIDTH = 1280
IM_HEIGHT = 760

MODEL_NAME = 'models/faster_rcnn_inception_v2_coco_2018_01_28'
# MODEL_NAME = 'models/ssd_resnet50_v1_fpn_shared_box_predictor_640x640_coco14_sync_2018_07_03'

DYNAMODB_TABLE_NAME = 'grey-rose-rooms-status'
AWS_PROFILE = f'{HOST_NAME}-pi'