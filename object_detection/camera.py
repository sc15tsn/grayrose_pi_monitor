from picamera import PiCamera


class Camera:

    def __init__(self, resolution):
        self.camera = PiCamera()
        self.camera.resolution = resolution

    def take_picture(self, out):
        self.camera.capture(out)

    def record_video(self, out, duration):
        self.camera.start_recording(out)
        self.camera.wait_recording(duration)
        self.camera.stop_recording()

    def close(self):
        self.camera.close()
