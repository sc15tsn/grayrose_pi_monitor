from datetime import datetime
import boto3
from botocore.exceptions import ClientError
from config import ROOM_NAME, AWS_PROFILE


class DynamoDBClient:

    def __init__(self, region, table_name):
        session = boto3.session.Session(profile_name=AWS_PROFILE)
        db = session.resource('dynamodb', region_name=region)
        self.table = db.Table(table_name)

    def  put_room_status(self, room_status):
        print(f'Updating status of {ROOM_NAME} with status: is_empty: {room_status}')
        current_time = datetime.now().strftime('%H:%M:%S')
        self.table.put_item(Item={
            'room_name': ROOM_NAME,
            'is_empty': room_status,
            'last_updated': current_time
        })
