### This script takes a picture using picamera and then evaluates whether a
### person is in the image using the tensorflow object detection API.
#
# The code is adapted from:
# https://github.com/EdjeElectronics/TensorFlow-Object-Detection-on-the-Raspberry-Pi

import os
import sys
import time


import cv2
import numpy as np

from camera import Camera
from config import (IM_WIDTH, IM_HEIGHT, MODEL_NAME, DYNAMODB_TABLE_NAME)
from aws import DynamoDBClient
from room_status_evaluators import ObjectDetectionApi


sys.path.append('..')

img_path = 'res/img.jpg'
camera = Camera((IM_WIDTH, IM_HEIGHT))
camera.take_picture(img_path)
camera.close()

object_detector = ObjectDetectionApi(MODEL_NAME)

print('Starting timer...')
start = time.time()

person_detected = object_detector.is_room_empty(img_path)
os.remove(img_path)

print(f'Image evaluated in {time.time() - start}s')

dynammodb_client = DynamoDBClient(
    region='eu-west-2',
    table_name=DYNAMODB_TABLE_NAME
)

if person_detected:
    dynammodb_client.put_room_status('false')
else:
    dynammodb_client.put_room_status('true')

print(f'Process complete! in {time.time() - start}s')
exit()