from abc import abstractmethod
import os

from PIL import Image
import cv2
import numpy as np
import tensorflow as tf
from utils import label_map_util

class RoomStatusEvaluator:

    @abstractmethod
    def is_room_empty(self, room_image_path):
        pass

    @staticmethod
    def prepare_image_for_evaluator(room_image_path):
        im = Image.open(room_image_path)
        im = np.asarray(im)
        im_rgb = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        return np.expand_dims(im_rgb, axis=0)


class ObjectDetectionApi(RoomStatusEvaluator):

    def __init__(self, tf_model_name):
        cwd_path = os.getcwd()
        path_to_checkpoint = os.path.join(cwd_path, tf_model_name, 'frozen_inference_graph.pb')
        self.detection_graph = tf.Graph()
        self.sess = self._init_tf_session(path_to_checkpoint)

        path_to_labels = os.path.join(cwd_path, 'data', 'mscoco_label_map.pbtxt')
        NUM_CLASSES = 90
        self.category_index = self._create_category_index(path_to_labels, NUM_CLASSES)
        self.output_tensors = self._init_output_tensors()
        self.confidence_threshold = 0.3

    def is_room_empty(self, room_image_path):
        print('Running the image through the model...')
        (_, scores, classes, _) = self._run_model(room_image_path)
        scores = scores[0]
        classes = classes[0]
        is_room_empty = self._is_person_detected(classes, scores)
        if is_room_empty:
            print("There is a person in the room!")
        else:
            print("The room is empty!")
        return is_room_empty

    def _run_model(self, room_image_path):
        input_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        im_expanded = self.prepare_image_for_evaluator(room_image_path)
        return self.sess.run(
            self.output_tensors,
            feed_dict={input_tensor: im_expanded}
        )
    
    def _is_person_detected(self, classes, scores):
        for i in range(len(scores)):
            if self.category_index[classes[i]]['name'] == 'person':
                if scores[i] > self.confidence_threshold:
                    return True
        return False

    def _init_tf_session(self, path_to_checkpoint):
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(path_to_checkpoint, 'rb') as fid:
                serialised_graph = fid.read()
                od_graph_def.ParseFromString(serialised_graph)
                tf.import_graph_def(od_graph_def, name='')
            return tf.Session(graph=self.detection_graph)

    def _create_category_index(self, path_to_labels, num_classes):
        label_map = label_map_util.load_labelmap(path_to_labels)
        categories = label_map_util.convert_label_map_to_categories(
            label_map,
            max_num_classes=num_classes,
            use_display_name=True
        )
        return label_map_util.create_category_index(categories)

    def _init_output_tensors(self):
        tensors = []
        tensors.append(self.detection_graph.get_tensor_by_name('detection_boxes:0'))
        tensors.append(self.detection_graph.get_tensor_by_name('detection_scores:0'))
        tensors.append(self.detection_graph.get_tensor_by_name('detection_classes:0'))
        tensors.append(self.detection_graph.get_tensor_by_name('num_detections:0'))
        return tensors
        
