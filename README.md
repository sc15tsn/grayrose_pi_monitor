# GrayRose - Raspberry Pi Repository


### Currently deployed Raspberry Pis
| Asset Number | IP Address    | Hostname | Meeting Room |
| ------------ |---------------| ---------| ------------ |
| BJSS4674     | 10.161.153.12 | RSP4674  | Gray         |
| BJSS4675     | 10.161.153.22 | RSP4675  | Rose         |

## Required Hardware
- Raspberry Pi
- External mouse and keyboard (To set up raspberry pi)


### Setting up Ansible
We use [ansible](https://docs.ansible.com) to deploy to the raspberry pis. First, install ansible:
```sh
pip install ansible
```
Now, generate a keypair for your development machine
```sh
ssh-keygen -t rsa -b 4096
```

### Setting up the Rasperry Pi
1. Plug the raspberry pi into an ethernet port to connect to the BJSS network
2. Install the raspbian OS on the Raspbery Pi (e.g. by using a pre-formatted NOOBS SD card)
3. Update and upgrade the OS
```sh
sudo apt-get update
sudo apt-get upgrade
```

#### Changing the hostname of the pi
Follow [these](https://thepihut.com/blogs/raspberry-pi-tutorials/19668676-renaming-your-raspberry-pi-the-hostname) instructions to chanhge the hostname of your pi to the name of the room (eg rose or grey)


#### Creating a linux user
Once the raspberry pi is set up and running, we should create a user who's sole responsibility is to run the python program to repeatedly take pictures and upload to DynamoDB. Plug the raspberry pi into a monitor, mouse and keyboard. Log in as the pi user (default password is raspberry, this should be changed at the earliest opportunity).

1. Create the user and home directory
```sh
sudo useradd -m office-monitor
```
2. Add a password for the new user. Execute the following command and follow the prompts
```sh
sudo passwd office-monitor
```
3. Add the user to the video group to allow it to use the camera module
```sh
sudo usermod -a -G video office-monitor
```
4. Add the user to the sudoers group
```sh
sudo usermod -a -G sudo office-monitor
```
From this point, execute everything as the office-monitor user (login with ```su office-monitor```)

#### Setting up SSH
1 Enable and start SSH on the Pi
```sh
sudo systemctl enable ssh
sudo systemctl start ssh
```

2. Make a file on the Pi for authorized public keys
```sh
mkdir /home/office-monitor/.ssh 
touch /home/office-monitor/.ssh/authorized_keys
```

3. Copy the public key that we created on the local machine into the file. From the local machine execute:
```sh
scp ~/.ssh/{key_name}.pub office-monitor@{STATIC_IP_OF_PI}:/home/office-monitor/.ssh/authorized_keys
```

4. Edit the ssh config file on the Pi (/etc/ssh/sshd_config)
```sh
PasswordAuthentication no
PermitRootLogin no
```

5. Reboot the pi and execute this command on your local machine to test whether it worked:
```sh
 ssh office-monitor@{STATIC_IP_OF_PI} -i ~/.ssh/{key_name}
```
You should now be connected to the raspberry pi via ssh without having to input any password.

#### Set up the camera module
1. Connect the camera module to the Pi
2. Enable the camera module in the Pi configuration menu under Interfacing Options -> Camera
```sudo raspi-config```
(if the 'camera' option is not present in the menu, you may need to update and upgrade apt-get)
3. Reboot the Pi
4. The raspistill command can be used to test whether the camera is working
```sh
raspistill -o test.jpg
```

#### Install the tensorflow object detection api
This project uses the [tensorflow object detection api](https://github.com/tensorflow/models/tree/master/research/object_detection).
1. On your local machine download the code from this repository
```https://lapetus.bjss.com/James.Tait/raspberry-pi-object-detection.git```

2. Create a file ```ansible_inventory``` in the root of this project
```sh
touch ansible_directory
```
3. Fill it in with the usernames and IP addresses of the raspberry pis:
```sh
office-monitor@{STATIC_IP_OF_GREY_PI}
office-monitor@{STATIC_IP_OF_ROSE_PI}
```
4. Test the connection
```sh
ansible all -i ansible_inventory -m ping --private-key ~/.ssh/{key_name}
```

5. You should receive a response like below
```sh
aserver.example1.org | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
aserver.example2.org | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
```

6. Push the folder to the raspberry pis.
```sh
ansible-playbook --private-key ~/.ssh/{key_name} -i ansible_inventory ansible_deploy.yml
```
7. SSH into the raspberry pi as the office-monitor user and navigate to ```/home/office-monitor/raspberry-pi-object-detection```
8. Execute the following commands
```sh
sudo chmod +x setup.sh
./setup.sh
```
Where room_name is the name of the meeting room that the pi is set up in (rose or grey). This file installs all the required apt-get and python modules to run the object detection api. It will also install a model from the [tensorflow detection model zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md) and initialise the python variable file containing the room name. The command may take a while.

9. Configure the aws credentials after creating a user through the aws console
```sh
aws configure --profile {rose or grey}-pi
```

To test the installation - execute the following:
```sh
cd object_detection
python3 person_in_img.py
```

#### Running the object detection python script at startup
1. Create a directory to store the logs
```sh
mkdir /home/office-monitor/logs
```
2. We will use cron to run the python script at bootup. We need to edit the crontab of the root user
```sh
sudo crontab -e
```
3. Choose whichever editor you prefer and add this at the bottom of the file
```sh
* * * * * sh /home/office-monitor/raspberry-pi-object-detection/launch.sh > /home/office-monitor/logs/cronlog 2>&1
```
Now the pi will execute the python file every minute and dump the logs to ```home/office-monitor/logs/cronlog```. 
Now we are done with the raspberry Pi. Switch it off by unplugging it.
