HOST_NAME=$( cat /etc/hostname )
echo "HOST_NAME = '$HOST_NAME'" > object_detection/host_name.py

sudo apt-get -y update
sudo apt-get -y dist-upgrade
sudo apt-get -y install libatlas-base-dev
sudo apt-get -y install python-tk
sudo apt-get -y install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
sudo apt-get -y install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get -y install libxvidcore-dev libx264-dev
sudo apt-get -y install qt4-dev-tools libatlas-base-dev
sudo apt-get -y install protobuf-compiler


pip3 install -r ~/raspberry-pi-object-detection/requirements.txt --user

protoc object_detection/protos/*.proto --python_out=.

mkdir -p object_detection/res
mkdir -p object_detection/models

cd object_detection/models
wget http://download.tensorflow.org/models/object_detection/faster_rcnn_inception_v2_coco_2018_01_28.tar.gz
tar -xzvf faster_rcnn_inception_v2_coco_2018_01_28.tar.gz
rm -rf faster_rcnn_inception_v2_coco_2018_01_28.tar.gz

wget http://download.tensorflow.org/models/object_detection/ssd_resnet50_v1_fpn_shared_box_predictor_640x640_coco14_sync_2018_07_03.tar.gz
tar -xzvf ssd_resnet50_v1_fpn_shared_box_predictor_640x640_coco14_sync_2018_07_03.tar.gz
rm -rf ssd_resnet50_v1_fpn_shared_box_predictor_640x640_coco14_sync_2018_07_03.tar.gz

aws configure --profile "$HOST_NAME-pi"