#!/bin/sh

sudo -i -u office-monitor bash << EOF

cd /home/office-monitor/raspberry-pi-object-detection/object_detection
python3 person_in_img.py

EOF